#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder
from kivymd.uix.navigationdrawer import (
    MDNavigationDrawer, MDNavigationLayout, MDNavigationDrawerItem)
from kivy.properties import ObjectProperty
import gettext
from functools import partial
_ = partial(gettext.dgettext, 'tryvy')

Builder.load_string('''
<DrawerClickableItem>
    # by now it does not work
    #focus_color: "#333333"
    #text_color: "#4a4939"
    #icon_color: "#4a4939"
    #ripple_color: "#333333"
    #selected_color: "#333333"

<TrytonNavLayout>
    id: nav_layout
    root_app: app
    nav_drawer: nav_drawer
    orientation: 'vertical'

    TrytonScreenManager:
        id: smanager
        x: self.toolbar.height if self.toolbar else 0
        size_hint_y: 1.0 - ((self.toolbar.height if self.toolbar else 0)/root.parent.height)

    TrytonNavDrawer:
        id: nav_drawer

<TrytonNavDrawer>
    radius: (0, 16, 16, 0) if self.anchor == "left" else (16, 0, 0, 16)

    MDNavigationDrawerMenu:
        id: nav_menu

        MDNavigationDrawerHeader:
            id: nav_header
            source: app.drawer_image
            title: app.title
            text: app.drawer_text
            spacing: "20dp"
            padding: "4dp", 0, 0, "4dp"

        MDNavigationDrawerItem
            icon: "domain"
            text: app.context.get('company.rec_name') or _('<Define company>')
            on_release: app.create_many2one_company()

        MDNavigationDrawerDivider:
''')


class DrawerClickableItem(MDNavigationDrawerItem):
    pass


class TrytonNavLayout(MDNavigationLayout):
    root_app = ObjectProperty(None)


class TrytonNavDrawer(MDNavigationDrawer):
    _default_items = 2

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.menu_items_added = False
