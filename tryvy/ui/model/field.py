# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from .pyson import PYSONDecoder
from tryvy.tryton.call import execute as call_execute

DEFAULT_STATES = {u'readonly': False, u'invisible': False}


def format_record_values(values):
    def _format(_values, prefix=''):
        new_values = {}
        for key, value in _values.items():
            if key.endswith('.') and isinstance(value, dict):
                for subkey, subvalue in value.items():
                    if isinstance(subvalue, dict):
                        new_values.update(_format(subvalue, key + subkey))
                    else:
                        new_values[prefix + key + subkey] = subvalue
            else:
                new_values[prefix + key] = value
        return new_values

    if isinstance(values, dict):
        return _format(values)
    elif isinstance(values, list):
        return [format_record_values(value) for value in values]


class Field(object):

    def __init__(self, value=None, states={}, model_name=None,
            domain={}, label='', field_type='', field_name=None, record=None,
            selection=[], digits=None, on_change_depends=[],
            on_change_with_depends=[], search_order=None, search_context=None,
            readonly=False):
        self._value = value
        self.type = field_type
        self.label = label
        self.modified = False
        self.field_name = field_name
        self.record = record
        self.readonly = readonly
        self._states = states
        self._domain = domain
        self.model_name = model_name
        self.selection = selection
        self.digits = digits
        self.on_change_depends = on_change_depends
        self.on_change_with_depends = on_change_with_depends
        self._search_order = search_order
        self._search_context = search_context

    @property
    def states(self):
        if not self._states:
            return DEFAULT_STATES
        states = PYSONDecoder(self.context).decode(self._states)
        default_states = dict(DEFAULT_STATES)
        default_states.update(states)
        return default_states

    @states.setter
    def states(self, new_states):
        self._states = new_states

    @property
    def domain(self):
        if not self._domain:
            return {}
        return PYSONDecoder(self.context).decode(self._domain)

    @domain.setter
    def domain(self, new_domain):
        self._domain = new_domain

    @property
    def value(self):
        if self._value is None or self._value == 'None':
            return ''
        return self._value

    @property
    def convert_value(self):
        # TODO: unify with value property
        if self.type in {
                'integer', 'float', 'numeric', 'datetime', 'date',
                'time', 'many2one'}:
            if self._value in {'', 'None', None}:
                return None
            return self._value
        if self.type == 'boolean':
            if self._value in ('', 'None', None):
                return False
            return self._value
        return self.value

    def get_hours(self):
        if self.type == 'timedelta':
            return self._calculate_duration(self.value)

    def get_minutes(self):
        if self.type == 'timedelta':
            return self._calculate_duration(self.value, type_='minutes')

    def _calculate_duration(self, duration, type_='hours'):
        if duration:
            hours, remainder = divmod(duration.seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            if type_ == 'hours':
                return hours
        else:
            return ''
        return minutes

    def set_not_modified(self):
        self.modified = False
        del self.record.modified_fields[self.field_name]

    @value.setter
    def value(self, new_value):
        self._value = new_value
        if self.record.state in {'edit', 'create'}:
            self.modified = True
            self.record.context.update({self.field_name: self._value})
            self.record.values.update({self.field_name: self._value})
            self.record.modified_fields.setdefault(self.field_name)

    @property
    def context(self):
        return self.record.context

    @property
    def search_context(self):
        context = self.context
        context.update(self.record.expr_eval(self._search_context or {}))
        return context

    @property
    def search_order(self):
        order = self.record.expr_eval(self._search_order)
        return order

    def get_attribute(self, attribute):
        if self.value and attribute:
            return call_execute('model', self.model_name, 'read',
                [self.value], [attribute])[0][attribute]

    def _get_on_change_args(self, field_name, depends):
        values = {}
        for depend in depends:
            if not depend.startswith('_parent_'):
                values[depend] = self.record[depend].value or None
        values['id'] = self.record['id'].value
        return values

    def on_change(self):
        if not self.on_change_depends:
            return
        values = self._get_on_change_args(self.field_name,
                self.on_change_depends)
        if values:
            try:
                changes = call_execute('model', self.record.model_name,
                    'on_change', values, [self.field_name],
                    context=self.context['context'])
            except Exception:
                return
        return format_record_values(changes)

    def on_change_with(self):
        if not self.on_change_with_depends:
            return
        values = self._get_on_change_args(self.field_name,
            self.on_change_with_depends)
        if values:
            try:
                changes = call_execute('model', self.record.model_name,
                    'on_change_with', values, [self.field_name],
                    context=self.context['context'])
            except Exception:
                return
        return changes

    def __repr__(self):
        return 'Field(states={}, domain={}, '\
            'label={}, model_name={}, type={}, '\
            'field_name={})\n\n'.format(
                self.states, self.domain, self.label.encode('utf-8'),
                self.model_name, self.type, self.field_name)
