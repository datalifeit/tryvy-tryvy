#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder
from kivy.properties import (StringProperty, ObjectProperty, BooleanProperty,
    ListProperty)
from tryvy.ui.tree import TrytonTree, TreeOneLineAvatarItem, TreeItemFactory
from tryvy.ui.screen import TrytonTreeScreen

Builder.load_string("""
<Many2One>
    Many2OneTree

<Many2OneItem>:
    text: self.record['rec_name'].value
""")


class Many2OneItem(TreeOneLineAvatarItem):
    pass


class Many2OneTree(TrytonTree):
    _limit = 30
    _fields = ['rec_name']
    _search_context = {}

    def _get_record_domain(self):
        res = super(Many2OneTree, self)._get_record_domain()
        if self.tryton_screen.relation_domain:
            res.extend(self.tryton_screen.relation_domain)
        return res

    def show_record(self, record, context=None):
        self.tryton_screen.set_value(record)
        if self.tryton_screen.go_back_on_set:
            self.tryton_screen.on_go_back()

    def get_model_name(self):
        return self.tryton_screen.model_name

    @property
    def context(self):
        ctx = super().context
        if self._search_context:
            ctx.update(self._search_context)
        return ctx


class Many2OneMixin(TrytonTreeScreen):
    model_name = StringProperty(None)
    value = ObjectProperty(None)
    go_back_on_set = BooleanProperty(True)
    # TODO: temp solution
    relation_name = StringProperty('')
    relation_domain = ListProperty([])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.filterbar_type = 'Text'
        self.register_event_type('on_set_value')
        self.relation_field = kwargs.get('relation_field')

    def configure_relation(self, relation_field=None, prev_screen=None):
        if not self.relation_field and relation_field:
            self.relation_field = relation_field
        self.relation_name = self.relation_field.field_name
        if prev_screen is not None:
            self.prev_screen = prev_screen
        if self.relation_field.field.domain:
            self.relation_domain = self.relation_field.field.domain
        if self.relation_field.field.search_order:
            self.tryton_view._order = self.relation_field.field.search_order
        if self.relation_field.field.search_context:
            self.tryton_view._search_context = \
                self.relation_field.field.search_context

    def on_pre_enter(self):
        super().on_pre_enter()
        if self.relation_field:
            screen = self.manager.get_screen(self.prev_screen)
            screen.fbind('on_pre_enter', self.relation_field.clear_screen,
                self, screen)

    def _set_value(self, new_value):
        if self.relation_name:
            assert self.prev_screen
            screen = self.manager.get_screen(self.prev_screen)
            screen.tryton_view.record[
                self.relation_name].value = new_value['id'].value
            screen.tryton_view.record[self.relation_name +
                '.rec_name'].value = new_value['rec_name'].value
            screen.tryton_view.rebind_record()

    def set_value(self, new_value):
        self.value = new_value
        self._set_value(new_value)
        self.dispatch('on_set_value')

    def on_set_value(self, *args):
        if self.relation_field:
            self.relation_field.dispatch('on_set_value')


class Many2One(Many2OneMixin):
    pass


TreeItemFactory().register({
    Many2OneTree.__name__: Many2OneItem
    })
