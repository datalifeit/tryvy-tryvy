#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from datetime import timedelta
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, AliasProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.app import MDApp

from tryvy.misc import unformat_number, format_number, format_date
from tryvy.ui.dialog import ErrorDialog
# needed for dynamic Selection items
from tryvy.ui.view.selection import Selection
from ..form import FormItem
from functools import partial
import gettext
from decimal import Decimal
from tryvy.filler_init import FillerInit
from kivymd.uix.pickers import MDDatePicker
from ..picker import MDTimePicker
from tryvy.common.common import untimezoned_date, timezoned_date

_ = partial(gettext.dgettext, 'tryvy')

Builder.load_string("""
#:import format_date tryvy.misc.format_date
#:import MDDropdownMenu kivymd.uix.menu.MDDropdownMenu

<TrytonIcon>
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        icon: root.field_icon
        size_hint_x: None
        disabled: root.root_widget.record[root.field_name].states['readonly'] \
            or root.readonly \
            or not root.root_widget.perm_modify

<TrytonM2OTextField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
        'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        multiline: root.multiline
        text: root.root_widget.record[root.field_rec_name].value
        hint_text: root.root_widget.record[root.field_name].label + ':'
        disabled: True
        on_text: root.on_change_text(self.text)
        size_hint_y: None if self.height or self.height <= root.height else 1
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        icon: 'magnify' if not root.root_widget.record[\
            root.field_name].value else 'close'
        disabled: not root.opacity or root.readonly
        opacity: 0 if root.readonly else 1
        size_hint_x: 0 if root.readonly else None
        on_press:
            root.configure_relation()

<TrytonDate>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        disabled: root.readonly
        icon: root.field_icon
        size_hint_x: None
    MDTextField:
        disabled: True
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: format_date(root.root_widget.record[root.field_name].value)
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        icon: 'magnify'
        disabled: not root.opacity or root.readonly
        size_hint_x: 0 if root.readonly else None
        on_press: root.show_date_picker()

<TrytonTextField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        multiline: root.multiline
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: str(root.root_widget.record[root.field_name].value)
        disabled: not root.opacity or root.readonly
        on_focus:
            root.set_value(self, root.field_name)

<TrytonNumericField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        input_type: 'number'
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: format_number(root.root_widget.record[\
            root.field_name].value)
        disabled: not root.opacity or root.readonly
        on_focus:
            root.set_value(self, root.field_name)
    MDTextField:
        text: root.root_widget.record[\
            root.field_symbol].value if root.field_symbol else ''
        size_hint_x: .2 if root.field_symbol else 0

<TrytonIntField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        input_type: 'number'
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: format_number(root.root_widget.record[\
            root.field_name].value)
        disabled: not root.opacity or root.readonly
        on_focus:
            root.set_value(self, root.field_name)

<TrytonFloatField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        input_type: 'number'
        size_hint_x: 0.9 if root.field_symbol else 1
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: format_number(root.root_widget.record[\
            root.field_name].value)
        disabled: not root.opacity or root.readonly
        on_focus:
            root.set_value(self, root.field_name)
    MDTextField:
        text: root.root_widget.record[\
            root.field_symbol].value if root.field_symbol else ''
        size_hint_x: .1 if root.field_symbol else 0
        disabled: 1

<TrytonSelection>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        disabled: True
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: root.format_value(root.root_widget.record[root.field_name].value)
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        icon: 'magnify'
        disabled: not root.opacity or root.readonly
        size_hint_x: 0 if root.readonly else None
        opacity: 0 if root.readonly else 1
        on_release: MDDropdownMenu(\
            caller=self, items=root.type_items(), width_mult=4).open()

<TrytonDateTime>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    MDTextField:
        id: text_field
        disabled: True
        hint_text: root.root_widget.record[root.field_name].label + ':'
        text: format_date(root.root_widget.record[root.field_name].value)
    MDIconButton:
        md_bg_color_disabled: self.md_bg_color
        icon: 'magnify'
        disabled: not root.opacity or root.readonly
        size_hint_x: 0 if root.readonly else None
        on_press: root.show_time_picker()

<TrytonTimeDelta>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    orientation: 'vertical'
    BoxLayout:
        orientation: 'vertical'
        MDLabel:
            font_style: 'Subtitle1'
            theme_text_color: 'Primary'
            text: root.root_widget.record[root.field_name].label
            halign: 'left'
        BoxLayout:
            MDTextField:
                input_type: 'number'
                hint_text: _('Hours:')
                text: format_number(root.root_widget.record[root.field_name].get_hours())
                disabled: not root.opacity or root.readonly
                on_focus:
                    root.set_value(self, 'hours')
            MDTextField:
                input_type: 'number'
                hint_text: _('Minutes:')
                text: format_number(root.root_widget.record[root.field_name].get_minutes())
                disabled: not root.opacity or root.readonly
                on_focus:
                    root.set_value(self, 'minutes')

<TrytonBoolField>
    opacity: 0 if self.root_widget.record[self.field_name].states[\
       'invisible'] else 1
    size_hint_y: 1 if self.opacity else 0
    on_size_hint_y: root.on_change_size_hint_y(self.size_hint_y)
    readonly: self.root_widget.record[self.field_name].readonly \
        or self.root_widget.record[self.field_name].states['readonly'] \
        or not self.root_widget.perm_modify
    disabled: not self.opacity or self.readonly
    MDLabel:
        font_style: 'Subtitle1'
        theme_text_color: 'Primary'
        text: root.root_widget.record[root.field_name].label
        halign: 'left'
    MDCheckbox:
        active: root.root_widget.record[root.field_name].value
        on_state:
            root.set_value(self, root.field_name)
            root.on_change_text(self.active)
""")


class FieldMixin(FillerInit):
    field_name = StringProperty('')
    readonly = BooleanProperty(False)

    def get_root_widget(self):
        parent = self.parent
        while parent:
            if isinstance(parent, FormItem):
                return parent
            parent = parent.parent
        return None

    root_widget = AliasProperty(get_root_widget, rebind=True)

    def __init__(self):
        self.reductions = {}
        self.modified = False
        # TODO registrar cada field en el record para evitar tener
        # que manualmente definir el _fields

    @property
    def field_rec_name(self):
        return "{}.rec_name".format(self.field_name)

    @property
    def record(self):
        return self.root_widget.record

    @property
    def field(self):
        return self.record[self.field_name]

    def on_change_size_hint_y(self, new_value):
        if new_value == 0 and self.field_name not in self.reductions:
            self.parent.height -= self.height
            self.reductions[self.field_name] = self.height
        else:
            self.parent.height += self.reductions[self.field_name]
            del self.reductions[self.field_name]

    def is_modified(self):
        return self.modified or self.root_widget.record.state == 'create'

    def set_text(self, new_value):
        self.field.value = new_value

    def on_change(self):
        changes = self.record.on_change(self.field_name)
        if not changes:
            return
        self.set_on_change(changes)

    def set_on_change(self, changes):
        for change in changes:
            for field_name, value in change.items():
                self.record[field_name].value = value

    @property
    def client_value(self):
        return self.field.value


class TrytonM2OTextField(BoxLayout, FieldMixin):

    multiline = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(TrytonM2OTextField, self).__init__(**kwargs)
        self.register_event_type('on_set_value')

    def on_change_text(self, new_value):
        # Many2One fields are updated at the _set_value method
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        if self.is_modified():
            self.on_change()
        self.modified = True

    def configure_relation(self):
        # TODO: fails if import at beginning
        from tryvy.ui.view.many2one import Many2One

        root_app = MDApp.get_running_app()
        self.modified = True
        if not self.field.value:
            m2o_screen = Many2One(
                name='m2o_%s' % self.field_name,
                title=self.root_widget.record[self.field_name].label,
                model_name=self.field.model_name,
                relation_field=self)
            m2o_screen.configure_relation(
                prev_screen=root_app.nav_layout.ids.smanager.current_screen.name)
            if root_app.nav_layout.ids.smanager.has_screen(m2o_screen.name):
                screen = root_app.nav_layout.ids.smanager.get_screen(
                    m2o_screen.name)
                root_app.nav_layout.ids.smanager.remove_widget(screen)
            root_app.nav_layout.ids.smanager.add_widget(m2o_screen)
            root_app.nav_layout.ids.smanager.on_go_to(m2o_screen.name)
        else:
            self.field.value = None
            self.root_widget.record[self.field_rec_name].value = None
            self.root_widget.rebind_record()

    def clear_screen(self, m2oscreen, screen, *args):
        screen.manager.remove_widget(m2oscreen)
        screen.funbind('on_pre_enter', self.clear_screen, m2oscreen, screen)

    def on_set_value(self, *args):
        pass


class TrytonIcon(object):
    field_icon = StringProperty('account-box-outline')


class TrytonDate(BoxLayout, FieldMixin):
    field_icon = StringProperty('calendar')

    def show_date_picker(self):
        _date = None
        if self.root_widget.record[self.field_name]:
            _date = self.root_widget.record[self.field_name].value
        self.date_dialog = MDDatePicker(
            year=_date.year if _date else None,
            month=_date.month if _date else None,
            day=_date.day if _date else None,
            title=_('SELECT DATE'),
            title_input=_('INPUT DATE'))
        self.date_dialog.bind(on_save=self.on_picker_save)
        self.date_dialog.open()

    def on_picker_save(self, instance, value, date_range):
        self.field.value = value
        self.root_widget.rebind_record()


class TrytonTextField(BoxLayout, FieldMixin):

    multiline = BooleanProperty(False)

    def set_value(self, instance, field_name):
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        if instance.focus:
            return
        if instance.text != self.field.value:
            self.modified = True
        if not self.is_modified():
            return
        self.set_text(instance.text)
        self.on_change()
        self.modified = False


class TrytonNumericMixin(FieldMixin):
    field_symbol = StringProperty('')

    def set_value(self, instance, field_name):
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        if instance.focus or not instance.text:
            return
        if instance.text != self.client_value:
            self.modified = True
        if not self.is_modified():
            return
        try:
            self.set_text(instance.text)
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Format error!'),
                    text=_('"%s" is not a valid number!' % instance.text),
                    exception=e)
            return
        self.on_change()
        self.modified = False

    def set_text(self, new_value):
        if not new_value:
            return
        self.field.value = self.convert(new_value)

    def convert(self, value):
        pass

    @property
    def client_value(self):
        return format_number(self.field.value)


class TrytonIntField(BoxLayout, TrytonNumericMixin):

    def convert(self, value):
        return int(unformat_number(value))


class TrytonSelection(BoxLayout, FieldMixin):

    def type_items(self):
        return [{'viewclass': 'Selection',
            'text': _(label),
            'record': value,
            'callback': self.select_type}
            for value, label in self.field.selection]

    def select_type(self, instance, value):
        self.field.value = value
        self.on_change()
        self.root_widget.rebind_record()

    def format_value(self, value):
        for selection_value, label in self.field.selection:
            if selection_value == value:
                return label
        return ''

    @property
    def client_value(self):
        return self.format_value(self.field.value)


class TrytonNumericField(BoxLayout, TrytonNumericMixin):

    def convert(self, value):
        return Decimal(unformat_number(value))


class TrytonFloatField(BoxLayout, TrytonNumericMixin):

    def convert(self, value):
        return unformat_number(value)


class TrytonIconIntField(TrytonIntField, TrytonIcon):
    pass


class TrytonIconTextField(TrytonTextField, TrytonIcon):
    pass


class TrytonBoolField(BoxLayout, FieldMixin):

    def set_value(self, instance, field_name):
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        if instance.active != self.field.value:
            self.modified = True
        if not self.is_modified():
            return
        self.set_text(instance.active)
        self.on_change()
        self.modified = False

    def set_text(self, new_value):
        self.field.value = new_value

    def on_change_text(self, new_value):
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        if self.is_modified():
            self.on_change()
        self.modified = True


class TrytonM2OIconField(TrytonM2OTextField, TrytonIcon):
    pass


class TrytonDateTime(BoxLayout, FieldMixin):

    def show_time_picker(self):
        _time = self.client_value and self.client_value.time() or None
        self.time_dialog = MDTimePicker()
        if _time:
            self.time_dialog.set_time(_time)
        self.time_dialog.bind(on_save=self.get_time_picker_data)
        self.time_dialog.open()

    def get_time_picker_data(self, instance, time):
        self.field.value = untimezoned_date(self.client_value.replace(
            hour=time.hour, minute=time.minute))
        self.on_change()
        self.root_widget.rebind_record()

    @property
    def client_value(self):
        if isinstance(self.field.value, datetime.datetime) and \
                self.field.value:
            return timezoned_date(self.field.value)
        return self.field.value


class TrytonTimeDelta(BoxLayout, FieldMixin):

    def set_value(self, instance, field_name):
        if self.root_widget.record.state in {'deleted', 'show'}:
            return
        self.modified = True
        if instance.focus or not instance.text:
            return
        try:
            if field_name == 'hours':
                self.field.value = timedelta(
                    hours=float(unformat_number(instance.text)) or 0,
                    minutes=self.field.get_minutes() or 0)
            elif field_name == 'minutes':
                self.field.value = timedelta(
                    hours=self.field.get_hours() or 0,
                    minutes=float(unformat_number(instance.text)) or 0)
            self.on_change()
            self.root_widget.rebind_record()
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Format error!'),
                    text=_('"%s" is not a valid number!' % instance.text),
                    exception=e)
            return

    def set_text(self, new_value):
        if not new_value:
            return
        self.field.value = unformat_number(new_value)


# TODO dynamic tryton fields
TYPES = {
    'many2one': TrytonM2OTextField,
    'date': TrytonDate,
    'char': TrytonTextField,
    'int': TrytonIntField,
    'selection': TrytonSelection,
    'datetime': TrytonDateTime,
}
