from tryvy.tryton.call import CONTEXT


class TrytonContext(object):

    @property
    def context(self):
        context = dict(CONTEXT)
        parent_context = {}
        if self.parent:
            parent_context = getattr(self.parent, 'context', {})

        if parent_context:
            context.update(parent_context)
        return context
