#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder
from kivymd.uix.toolbar import MDTopAppBar

Builder.load_string('''
<TrytonToolbar>
    pos_hint: {"top": 1}
    elevation: 2
''')


class TrytonToolbar(MDTopAppBar):
    pass
