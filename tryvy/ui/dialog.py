#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from functools import partial
from kivy.metrics import dp
import gettext

_ = partial(gettext.dgettext, 'tryvy')


class ErrorDialog(object):
    dialog = None

    def open(self, title, text='', width=.8, height=200, exception=None,
            auto_dismiss=False):
        assert (text or exception)
        if exception:
            exception_message = ''
            if getattr(exception, 'message', None):
                exception_message = exception.message
            elif exception.args:
                args = list(exception.args)
                if isinstance(args[0], int):
                    args = args[1:]
                exception_message = args[0]
            else:
                exception_message = exception
            if text:
                text += '\n' + str(exception_message)
            else:
                text = str(exception_message)
        self.dialog = MDDialog(
            title=title,
            type='alert',
            text=text,
            buttons=[MDFlatButton(text=_('OK'),
                on_release=lambda *x: self.close())],
            size_hint=(width, None),
            height=dp(height),
            auto_dismiss=auto_dismiss)
        self.dialog.open()

    def close(self, *args):
        if self.dialog:
            self.dialog.dismiss(force=True)


class ConfirmDialog(object):
    dialog = None
    confirm_callback = None

    def open(self, title, text, on_accept):
        self.confirm_callback = on_accept

        self.dialog = MDDialog(
            title=title,
            text=text,
            type='simple',
            size_hint=(.8, None),
            height=dp(200),
            auto_dismiss=False,
            buttons=[
                MDFlatButton(text=_('YES'),
                    on_release=lambda *x: self.confirm_action(True)),
                MDFlatButton(text=_('NO'),
                    on_release=lambda *x: self.confirm_action(False)),
            ])
        self.dialog.open()

    def confirm_action(self, result):
        self.dialog.dismiss()
        self.confirm_callback(result)
