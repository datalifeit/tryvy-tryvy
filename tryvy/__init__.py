#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
__version__ = "2.1.3"
__description__ = "Set of widgets for Kivy to create Tryton apps"
__name__ = "Tryvy"
