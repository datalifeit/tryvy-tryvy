import os
from functools import wraps
from kivy.app import App
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import ColorProperty
from kivy.uix.floatlayout import FloatLayout
from kivymd.utils import asynckivy
from kivymd.theming import ThemableBehavior

Builder.load_file(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', 'kv', 'spinner.kv')))


class AppSpinner(ThemableBehavior, FloatLayout):
    spinner_color = ColorProperty([1, 1, 1, 1])

    def start_anim_spinner(self):
        spinner = self.ids.body_spinner
        Animation(
            y=self.theme_cls.standard_increment + dp(10),
            d=0.4,
            t="out_expo",
        ).start(spinner)

    def hide_anim_spinner(self):
        spinner = self.ids.body_spinner
        anim = Animation(y=-dp(100), d=0.4, t="in_expo")
        anim.bind(on_complete=self.reset_spinner)
        anim.start(spinner)

    def reset_spinner(self, *args):
        body_spinner = self.ids.body_spinner
        body_spinner.y = -dp(100)


def show_spinner(func):
    app = App.get_running_app()

    @wraps(func)
    def wrapper(cls, *args, **kwargs):
        def schedule_func(interval):
            async def async_func():
                await asynckivy.sleep(0)
                try:
                    func(cls, *args, **kwargs)
                finally:
                    app.disable_spinner()
            asynckivy.start(async_func())
        app.enable_spinner()
        Clock.schedule_once(schedule_func, 1)
    return wrapper
