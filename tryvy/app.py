#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.properties import (StringProperty, ObjectProperty, BooleanProperty,
    AliasProperty, DictProperty)
from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder
from kivy.utils import platform
from kivy.core.window import Window

from .ui.navdrawer import DrawerClickableItem
from .ui.screen import TrytonTreeScreen
from .ui.dialog import ErrorDialog, ConfirmDialog
from .tryton.call import login as call_login, logout as call_logout, \
    execute as call_execute, CONTEXT
from .misc import NOT_ANDROID
from .translate import setlang, set_language_direction
from .common.common import settz, ContextReload
from .config import CONFIG
from functools import partial
from plyer import gps
from .ui.view.many2one import Many2One
from kivymd.uix.navigationdrawer import (
    MDNavigationDrawerLabel, MDNavigationDrawerDivider)
import os
import sys
import traceback
import gettext
from pathlib import Path
from kivy.clock import mainthread, Clock
from kivymd.app import MDApp

_ = partial(gettext.dgettext, 'tryvy')

if getattr(sys, "frozen", False):  # bundle mode with PyInstaller
    os.environ["TRYVY_ROOT"] = sys._MEIPASS
else:
    os.environ["TRYVY_ROOT"] = str(Path(__file__).parent)


if platform == 'android':
    from android.broadcast import BroadcastReceiver
    from jnius import autoclass, cast
    PythonActivity = autoclass('org.kivy.android.PythonActivity')
    Intent = autoclass('android.content.Intent')

__all__ = ['TrytonLayout', 'TrytonApp', 'toggle_drawer']


class TrytonLayout(FloatLayout):
    nav_layout = ObjectProperty(None)

    _callback_btn_float = None

    def configure_float_btn(self, callback, icon='plus'):
        self._callback_btn_float = callback
        self.ids.float_btn.icon = icon or 'plus'
        self.ids.float_btn.opacity = 1 if callback else 0
        self.ids.float_btn.disabled = (False
            if callback else True)

    def on_press_float_btn(self):
        if self._callback_btn_float:
            self._callback_btn_float()


def toggle_drawer(f):
    def wrapper(*args, **kwargs):
        app = args[0]
        app.toggle_nav_drawer()
        return f(*args, **kwargs)
    return wrapper


class TrytonApp(MDApp):
    server = StringProperty('')
    secondary_server = StringProperty('')
    port = StringProperty('')
    secondary_port = StringProperty('')
    database = StringProperty('')
    uid = StringProperty('')
    pwd = StringProperty('')
    repl_interval = StringProperty('')
    root_layout = ObjectProperty(None)
    nav_layout = ObjectProperty(None)
    logged = BooleanProperty(False)
    title = StringProperty('')
    login_text = StringProperty('')
    _textdomains = []
    company_info = ''
    company_version = ''
    company_description = ''
    gps_location = DictProperty({})
    gps_compatible = BooleanProperty(True)
    drawer_image = StringProperty('')
    drawer_text = StringProperty('')
    login_image = StringProperty('')

    def __init__(self, persist_mode='REMOTE', replication=False,
            database=None):
        assert persist_mode in ('REMOTE', 'LOCAL')
        assert not (replication and persist_mode == 'REMOTE')

        setlang(lang=CONFIG['client.lang'],
            extra_textdomains=self._textdomains)
        settz()
        super(TrytonApp, self).__init__()
        self._persist_mode = persist_mode
        self.replication = replication
        self.repl_database = database
        self.register_event_type('on_logged')

    def build(self):
        self._get_config()
        Window.softinput_mode = 'below_target'
        Window.bind(on_keyboard=self._key_handler)
        try:
            gps.configure(on_location=self.on_location)
        except NotImplementedError:
            self.gps_compatible = False
            print("Warning: GPS is not implemented for your platform")
        if platform == 'android':
            from android.permissions import (
                request_permissions, check_permission)
            perms = self._get_permissons_to_request()
            if not all([check_permission(perm) for perm in perms]):
                request_permissions(perms)
        Builder.load_file(
            os.path.join(os.path.dirname(__file__), 'libs', 'kv', 'root.kv'))
        # Must create an return a Widget
        return None

    def _get_permissons_to_request(self):
        return []

    @mainthread
    def on_location(self, **kwargs):
        self.gps_location['longitude'] = kwargs.get('lon')
        self.gps_location['latitude'] = kwargs.get('lat')

    def start(self, minTime, minDistance):
        gps.start(minTime, minDistance)

    def stop(self):
        gps.stop()

    def create_menus(self):
        nav_drawer = self.nav_layout.nav_drawer
        items = self.get_drawer_box_items()
        if not nav_drawer.menu_items_added:
            for icon, text, callback in items:
                if icon == 'label':
                    item = MDNavigationDrawerLabel(text=text)
                elif icon == 'divider':
                    item = MDNavigationDrawerDivider()
                else:
                    item = DrawerClickableItem(text=text, icon=icon)
                if callback:
                    item.bind(on_release=callback)
                nav_drawer.ids.nav_menu.add_widget(item, 2)
            nav_drawer.menu_items_added = True

    def confirm_exit(self, result):
        if result:
            if platform == 'android':
                self.root_window.close()
            else:
                self.stop()

    def _key_handler(self, window, key, scancode, codepoint, modifier):
        # 1000 is "back" on Android
        # 27 is "escape" on computers
        # 1001 is "menu" on Android
        if key in (1001, 27):
            if self.nav_layout.state == 'open':
                self.toggle_nav_drawer()
            elif self.nav_layout.ids.smanager.current_screen.prev_screen:
                self.nav_layout.ids.smanager.current_screen.dispatch('on_go_back')
            else:
                dialog = ConfirmDialog()
                dialog.open(title=self.title,
                    text=_('Do you want to exit application?'),
                    on_accept=self.confirm_exit)
            return True
        elif key == 1001:
            self.dispatch('toggle_nav_drawer')
            return True
        else:           # the key now does nothing
            return False

    def _get_config(self):
        for item in ('server', 'port', 'secondary_server',
                'secondary_port', 'database', 'uid', 'pwd'):
            setattr(self, item,
                    self.config.getdefault('Settings', item, ''))

    def show_settings(self, current_screen):
        self.root.ids.settings_screen.create_data(self.config)
        self.root.ids.settings_screen.previous_screen = current_screen.name
        self.root.current = 'settings'

    @toggle_drawer
    def show_about(self):
        version = sys.modules['tryvy'].__version__
        description = sys.modules['tryvy'].__description__
        name = sys.modules['tryvy'].__name__
        text = (
            '{}\n'.format(name) + '=' * len(
                name) + '\n' + description + '\nVersion {}\n\n{}'.format(
                version, self.company_info + '=' * len(
                    self.company_info) + '\n' +
                self.company_description + 'Version {}'.format(
                        self.company_version))
        )
        dialog = ErrorDialog()
        dialog.open(title=_('About'), height=350, text=text, auto_dismiss=True)

    def get_company_params(self, m2o_screen):
        return {
            'company': m2o_screen.value['id'].value,
            'company.rec_name': m2o_screen.value['rec_name'].value
        }

    @toggle_drawer
    def create_many2one_company(self):
        m2o_screen = Many2One(
            name='company_prefs',
            title=_('Company'),
            model_name='company.company',
            filterbar_type='Text')
        smanager = self.nav_layout.ids.smanager
        if smanager.has_screen(m2o_screen.name):
            screen = smanager.get_screen(m2o_screen.name)
            smanager.remove_widget(screen)

        smanager.add_widget(m2o_screen)
        smanager.get_screen(
            'company_prefs').prev_screen = smanager.current
        m2o_screen.fbind('on_set_value', self.set_preferences)
        smanager.on_go_to('company_prefs')

    def build_config(self, config):
        config.setdefaults('Settings', {
            'server': '<server>',
            'port': 8000,
            'database': 'tryton',
            'uid': 'user',
            'pwd': None,
            'load_employees': True,
            'repl_interval': 60 * 5,
        })

    def close_settings(self):
        self._get_config()

    def on_config_change(self, config, section, key, value):
        if section == 'Settings':
            setattr(self, key, value)

    def save_config(self, **kwargs):
        self.nav_layout.ids.smanager.get_screen('settings').save_config(
            self.config, **kwargs)
        self._get_config()

    @toggle_drawer
    def logout(self):
        call_logout()
        self.set_logged(False)
        self.root.current = 'login'

    def login(self, repl_finished=False):
        if (
                not self.uid
                or not self.pwd
                or not self.server
                or not self.port
                or not self.database):
            return
        try:
            if repl_finished or call_login(self._persist_mode, self):
                self.get_preferences()

                Clock.schedule_once(lambda dt: self.set_logged(True), 0)
                return True
            else:
                login_screen = self.root.get_screen('login')
                login_screen.in_progress = True
                self.login_text = _('Connecting to the server ...')
        except OSError:
            # database does not exists
            traceback.print_exc(file=sys.stdout)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            dialog = ErrorDialog()
            dialog.open(title=_('Connection error'),
                text=_('Cannot connect to server! '
                    'Please check the connection settings '
                    'and device connectivity.'),
                exception=e)
        return False

    def set_logged(self, value, *args):
        self.logged = value

    def on_logged(self, *args):
        self.root.get_screen('login').in_progress = False
        self.create_menus()
        self.root.current = 'root_screen'
        next_screen = self._get_next_login_screen()
        self.nav_layout.ids.smanager.current = next_screen
        screen = self.nav_layout.ids.smanager.get_screen(next_screen)
        screen.configure_toolbar()

    def _get_next_login_screen(self):
        return 'home'

    def get_preferences(self):
        def _set_preferences(prefs):
            try:
                prefs = prefs()
            except Exception:
                prefs = {}
            if prefs and 'language_direction' in prefs:
                set_language_direction(prefs['language_direction'])
                CONFIG['client.language_direction'] = \
                    prefs['language_direction']
            if prefs and 'language' in prefs:
                setlang(prefs['language'], prefs.get('locale'))
                if CONFIG['client.lang'] != prefs['language']:
                    self.set_menubar()
                    self.favorite_unset()
                CONFIG['client.lang'] = prefs['language']
            CONFIG.save()
            if self._persist_mode == 'LOCAL':
                CONTEXT.update({'replica_uid': self.uid})

        def _get_preferences():
            call_execute('model', 'res.user', 'get_preferences', False,
                callback=_set_preferences)

        ContextReload(_get_preferences)
        # rebind property
        prop = self.property('context')
        prop.dispatch(self)

    def get_context(self):
        return CONTEXT

    def set_preferences(self, m2o_screen):
        vals = self.get_company_params(m2o_screen)
        try:
            call_execute('model', 'res.user', 'set_preferences', vals)
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Server error'),
                text=_('Cannot set user preferences!'),
                exception=e)
        CONTEXT.update(vals)
        prop = self.property('context')
        prop.dispatch(self)
        current_screen = self.nav_layout.ids.smanager.current_screen
        if isinstance(current_screen, TrytonTreeScreen):
            current_screen.on_pre_enter()

    context = AliasProperty(get_context, rebind=True)

    def broadcast_handler(self, context, intent):
        # TODO: Filter using intent
        try:
            if self.replication:
                from .tryton.repl_service import activ_reader
                from .notify import PopNotifRequest
                activ_reader.wait_for_response(PopNotifRequest)
        except Exception:
            traceback.print_exc(file=sys.stdout)

    def _send_pause(self, value):
        from .tryton.repl_service import activ_reader, ActivityPausedRequest
        if activ_reader:
            activ_reader.wait_for_response(ActivityPausedRequest,
                data_array=[value], need_response=False)

    def on_pause(self):
        if self.replication:
            self._send_pause('1')
        gps.stop()
        return True

    def on_resume(self):
        if self.replication:
            self._send_pause('0')
        gps.start(1000, 0)

    def on_start(self):
        super(TrytonApp, self).on_start()
        if platform == 'android':
            self.br = BroadcastReceiver(self.broadcast_handler,
                ['provider_changed'])
            self.br.start()

    def on_stop(self):
        if platform in NOT_ANDROID and self.replication:
            from .tryton.repl_service import repl_stop
            repl_stop()
        Window.close()

    def replication_finished(self, model_names):
        screen = self.root.current_screen
        if self.logged and screen.name == 'root_screen':
            current_screen = self.nav_layout.ids.smanager.current_screen
            if isinstance(current_screen, TrytonTreeScreen):
                if current_screen.tryton_view._name in model_names:
                    Clock.schedule_once(
                        lambda x: current_screen.on_pre_enter())
        elif screen.name == 'login' and screen.in_progress:
            self.login(repl_finished=True)

    def replication_error(self, *args):
        if (self.root.current_screen.name != 'login'
                or not self.root.current_screen.in_progress):
            return
        dialog = ErrorDialog()

        def schedule_dialog(dt):
            self.root.current_screen.in_progress = False
            dialog.open(
                title=_('Connection error'),
                text=_('Cannot connect to server! '
                    'Please check the connection settings '
                    'and device connectivity.'),
                auto_dismiss=True)

        Clock.schedule_once(schedule_dialog, .5)

    def disable_spinner(self, dt=None):
        self.root_layout.ids.spinner.hide_anim_spinner()

    def enable_spinner(self, dt=None):
        self.root_layout.ids.spinner.start_anim_spinner()

    def toggle_nav_drawer(self):
        self.nav_layout.ids.nav_drawer.set_state('toggle')

    def get_drawer_box_items(self):
        items = [
            ('label', _('Forms'), None),
            ('divider', None, None),
            ('exit-to-app', _('Log out'), lambda *x: self.logout()),
            ('cog-outline', _('Settings'), lambda *x: self.show_settings(
                self.root.ids.root_screen)),
            ('information-variant', _('About'), lambda *x: self.show_about())
        ]
        if self.replication:
            items.insert(2, ('sync', _('Force Synchronization'),
                lambda *x: self.send_force_replication()))
        return items

    @toggle_drawer
    def go_to(self, instance, screen_name, context=None):
        if isinstance(instance, tuple):
            instance, = instance
        smanager = self.nav_layout.ids.smanager
        if (smanager.current == screen_name
                and context != smanager.current_screen.context):
            # additional screen change to force reload
            smanager.on_go_to('home')
        smanager.on_go_to(screen_name,
            title=instance.text, context=context)

    def force_replication(self, show_dialog=True,
            directions=None, models=None):
        if self.replication:
            from .tryton.repl_service import \
                _force_replicate, _send_notif_broadcast
            if not self.logged:
                return
            kwargs = {'models': models}
            if directions:
                kwargs['directions'] = directions
            success = _force_replicate(**kwargs)
            _send_notif_broadcast()
            current_screen = self.nav_layout.ids.smanager.current_screen
            if isinstance(current_screen, TrytonTreeScreen):
                Clock.schedule_once(
                    lambda x: current_screen.on_pre_enter())
            if success and show_dialog:
                dialog = ErrorDialog()
                dialog.open(
                    title=_('Synchronization finished'), height=350,
                    text=_('The synchronization process has finished. '
                        'Please check information is up-to-date.'),
                    auto_dismiss=True)

    def send_force_replication(self, directions=None, models=None):
        Clock.schedule_once(lambda dt: self.force_replication(
                show_dialog=False, directions=directions, models=models), 1)
