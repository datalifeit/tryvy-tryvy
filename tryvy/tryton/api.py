# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from kivy.utils import platform
from trytond.pool import Pool
from functools import wraps
from trytond.transaction import Transaction
from trytond import backend
from trytond.cache import Cache
from trytond.config import config

import sys
import traceback
import os


__all__ = ['with_pool', 'with_transaction', 'execute', 'init']

_USER_ID = 'admin'
_RETRY = 1

_db_name = None
config.update_etc('etc/trytond.conf')


def with_pool(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        database_list = Pool.database_list()
        pool = Pool(_db_name)
        if _db_name not in database_list:
            with Transaction().start(
                    _db_name, _USER_ID, readonly=True):
                pool.init()
        return func(pool, *args, **kwargs)
    return wrapper


def with_transaction(readonly=True):
    def decorator(func):
        @wraps(func)
        def wrapper(pool, *args, **kwargs):
            readonly_ = readonly  # can not modify non local
            for count in range(_RETRY, -1, -1):
                with Transaction().start(pool.database_name, 1,
                        readonly=readonly_) as transaction:
                    try:
                        Cache.clear_all()
                        result = func(*args, **kwargs)
                        return result
                    except backend.DatabaseOperationalError:
                        if count and not readonly_:
                            transaction.rollback()
                            continue
                        traceback.print_exc(file=sys.stdout)
                        raise
                    except Exception:
                        traceback.print_exc(file=sys.stdout)
                        raise

                    # Need to commit to unlock SQLite database
                    transaction.commit()

        return wrapper
    return decorator


def init(db_name, app_name):
    global _db_name
    if platform == 'android':
        from android.storage import app_storage_path
        _db_name = os.path.join(app_storage_path(), 'data', db_name)
    else:
        _db_name = db_name


def execute(*args, **kwargs):
    type_ = args[0]
    name = args[1]
    method_name = args[2]
    args = args[3:]
    if _get_transaction_readonly(name, method_name):
        return _execute_readonly(type_, name, method_name, *args, **kwargs)
    else:
        return _execute(type_, name, method_name, *args, **kwargs)


@with_pool
@with_transaction()
def _get_transaction_readonly(model_name, method_name, *args):
    _pool = Pool()
    Model = _pool.get(model_name)
    return Model.__rpc__.get(method_name).readonly


@with_pool
@with_transaction()
def _execute_readonly(type_, name, method_name, *args, **kwargs):
    return _api_execute(type_, name, method_name, *args, **kwargs)


@with_pool
@with_transaction(readonly=False)
def _execute(type_, name, method_name, *args, **kwargs):
    return _api_execute(type_, name, method_name, *args, **kwargs)


def _api_execute(type_, name, method_name, *args, **kwargs):
    pool = Pool()
    _context = {}
    args = list(args)
    kwargs = kwargs.copy()
    if 'context' in kwargs:
        _context.update(kwargs.pop('context'))
    with Transaction().set_context(**_context):
        Model = pool.get(name, type=type_)
        result = Model.__rpc__[method_name].result
        instantiate = Model.__rpc__[method_name].instantiate
        if instantiate is not None:

            def instance(data):
                if isinstance(data, int):
                    return Model(data)
                elif isinstance(data, dict):
                    return Model(**data)
                else:
                    return Model.browse(data)
            if isinstance(instantiate, slice):
                for i, data in enumerate(args[instantiate]):
                    start, _, step = instantiate.indices(len(args))
                    i = i * step + start
                    args[i] = instance(data)
            else:
                data = args[instantiate]
                args[instantiate] = instance(data)
        if method_name == 'write':
            return result(_write(Model, *args))
        elif method_name == 'delete':
            return result(_delete(Model, *args))
        else:
            _method = getattr(Model, method_name)
            return result(_method(*args, **kwargs))


def _write(Model, *args):
    iter_args = iter(args)
    write_args = []
    for model_ids, values in zip(iter_args, iter_args):
        write_args.append(Model.browse(model_ids))
        write_args.append(values)
    return Model.write(*write_args)


def _delete(Model, records):
    return Model.delete(Model.browse(records))
