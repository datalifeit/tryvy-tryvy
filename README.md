Tryvy
======

Tryvy is a collection of [Kivy](http://kivy.org) widgets, a framework for cross-platform, touch-enabled graphical applications.

The project's goal is to simplify the creation of tailored Tryton apps for Android devices using Kivy framework. 


Installation and use with Buildozer
===================================

To install Tryvy, clone the project and run the setup.py script. The following line works on Linux, other OS not tested:

    sudo ./setup.py install


License
=======

See LICENSE file
