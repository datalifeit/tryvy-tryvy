#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from setuptools import setup
import os
import io
import re


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def get_version() -> str:
    """Get __version__ from __init__.py file."""
    version_file = os.path.join(
        os.path.dirname(__file__), "tryvy", "__init__.py"
    )
    version_file_data = open(version_file, "rt", encoding="utf-8").read()
    version_regex = r"(?<=^__version__ = ['\"])[^'\"]+(?=['\"]$)"
    try:
        version = re.findall(version_regex, version_file_data, re.M)[0]
        return version
    except IndexError:
        raise ValueError(f"Unable to find version string in {version_file}.")


args = {}

try:
    from babel.messages import frontend as babel

    args['cmdclass'] = {
        'compile_catalog': babel.compile_catalog,
        'extract_messages': babel.extract_messages,
        'init_catalog': babel.init_catalog,
        'update_catalog': babel.update_catalog,
        }

    args['message_extractors'] = {
        'tryvy': [
            ('**.py', 'python', None),
            ('**.kv', 'python', None)],
        }

except ImportError:
    pass

package_data = {
    'tryvy': [
        'common/*.py',
        'libs/kv/*.kv',
        'libs/baseclass/*.py',
        'tryton/*.py',
        'ui/*.py',
        'ui/model/*.py',
        'ui/view/*.py',
        'data/locale/*/LC_MESSAGES/*.mo',
        'data/locale/*/LC_MESSAGES/*.po']
    }

setup(name='tryvy',
      version=get_version(),
      description='Set of widgets for Kivy to create Tryton apps',
      long_description=read('README.md'),
      author='Datalife S.Coop.',
      author_email='info@datalifeit.es',
      url='https://gitlab.com/datalifeit/tryvy-tryvy',
      license='GPL-3',
      packages=['tryvy'],
      package_data=package_data,
      install_requires=[
          'Kivy==2.2.1',
          'kivymd==1.0.2',
          'configparser',
          'babel',
          'python-dateutil'
      ],
      **args
      )
